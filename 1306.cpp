#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;
typedef long long ll;

unsigned int A[250 * 1000];

int main(){
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
		cin >> A[i];
 
	sort(A, A + n);
	
	if (n % 2 == 0)
		cout << setprecision(1) << fixed << 1.0 * (1.0 * A[n / 2 - 1] + 1.0 * A[n / 2]) / 2.0 << endl;
	else
		cout << setprecision(1) << fixed << 1.0 * A[(n + 1) / 2 - 1] << endl;
	return 0;
}