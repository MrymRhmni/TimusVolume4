// in his name
// Illusion has GONE!

#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#define pb push_back
using namespace std;
typedef pair<int, int> pii;

set <pii> seti[2];
vector <int> G[33000], ans;
int dis[2][33000], mark[2][33000];
int inf = 1000 * 1000 * 1000;

void make_dis(int v, int id){
	seti[id].erase(pii(inf, v));
	dis[id][v] = 0;
	seti[id].insert(pii(0, v));

	while (seti[id].size()){
		pii P = *seti[id].begin();
		seti[id].erase(seti[id].begin());
		if (P.first >= inf) return;
		v = P.second;
		mark[id][v] = 1;
		for (int i = 0; i < G[v].size(); i++){
			int u = G[v][i];
			if (!mark[id][u] and dis[id][u] > dis[id][v] + 1){
				seti[id].erase(pii(dis[id][u], u));
				dis[id][u] = dis[id][v] + 1;
				seti[id].insert(pii(dis[id][u], u));
			}
		}
	}
	return;
}

int main(){
	int t;
	cin >> t;

	while (t--){
		int n, a, b;
		cin >> n >> a;
		a--, n--;
		while(n --){
			cin >> b;
			b--;
			G[a].pb(b);
			G[b].pb(a);
			seti[0].insert(pii(inf, a));
			seti[0].insert(pii(inf, b));
			seti[1].insert(pii(inf, a));
			seti[1].insert(pii(inf, b));
			dis[0][a] = dis[0][b] = dis[1][a] = dis[1][b] = inf;
			a = b;
		}
	}

	int n, a, b;
	cin >> n >> a;
	a--;

	make_dis(a, 0);

	if (n == 1){
		for (int i = 0; i < 33000; i++){
			if (mark[0][i] and dis[0][i] != inf)
				ans.pb(i);
		}
	}

	else{
		n--;
		while (n--)
			cin >> b;

		b--;	

		make_dis(b, 1);

		for (int i = 0; i < 33000; i++){
			if (dis[0][i] == dis[1][i] + dis[0][b] and  mark[0][i] and mark[1][i] and dis[0][i] != inf)
				ans.pb(i);
		}
	}

	sort(ans.begin(), ans.end());

	for (int i = 0; i < ans.size(); i++)
		cout << ans[i] + 1 << '\n';

	return 0;
}
