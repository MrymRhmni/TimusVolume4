// in his name

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#define pb push_back
#define F first
#define S second
using namespace std;
typedef long long ll;
typedef pair<int, int> pi;
typedef pair<ll, pi> pii;

const int maxn = 505;
const ll inf = 1ll * 1000 * 1000 * 1000 * 1000 * 1000 * 100;
bool mark[maxn][maxn];
int n, m, a, b, c, d, A[maxn][maxn];
ll  dis[maxn][maxn];
// vector <pii> G[maxn][maxn];
set <pii> seti;

/*
void make_graph(){
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++){
			dis[i][j] = inf;
			seti.insert(pii(inf, pi(i, j)));
			if (A[i][j] == 0) continue;
			for (int x = -1; x <= 1; x++)
				for (int y = -1; y <= 1; y++){
					int W = 1;
					if ((x == 0 and y == 0) or A[i + x][j + y] == 0) continue;
					if (A[i + x][j + y] != A[i][j])
						W = 1000 * 1000;
					G[i][j].pb(pii(W, pi(i + x, j + y)));
					G[i + x][j + y].pb(pii(W, pi(i, j)));
				}
		}
}*/

void dijk(){

	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++){
			dis[i][j] = inf;
			seti.insert(pii(inf, pi(i, j)));
		}

	dis[a][b] = 1;
	seti.erase(pii(inf, pi(a, b)));
	seti.insert(pii(1, pi(a, b)));

	while (seti.size()){
		pii P = *seti.begin();
		seti.erase(seti.begin());
		if (P.F == inf) {cout << "0 0\n"; exit(0);}
		int i = P.S.F, j = P.S.S;
		if (i == c and j == d){
			cout << (P.F / (1ll * 1000 * 1000)) + (P.F % (1ll * 1000 * 1000)) 
			<< ' ' << (P.F / (1ll * 1000 * 1000)) << endl;
			exit(0);
		}
		mark[i][j] = 1;
		int ux = i,
			uy = j;


		for (int x = -1; x <= 1; x++)
			for (int y = -1; y <= 1; y++){
				int w = 1;
				if (A[i + x][j + y] == 0 or mark[i + x][j + y]) continue;
				
				if (A[i + x][j + y] != A[i][j])
					w = 1000 * 1000;

				int vx = i + x, 
					vy = j + y;

				if (dis[vx][vy] > dis[ux][uy] + w){
					seti.erase(pii(dis[vx][vy], pi(vx, vy)));
					dis[vx][vy] = dis[ux][uy] + w;
					seti.insert(pii(dis[vx][vy], pi(vx, vy)));
				}
			}
	}

}

int main(){
	cin >> n >> m >> a >> b >> c >> d;

	for (int i = 1; i <= n; i++){
		string St;
		cin >> St;
		for (int j = 1; j <= m; j++)
			A[i][j] = int(St[j - 1] - '0');
	}

	//make_graph();
	dijk();
}