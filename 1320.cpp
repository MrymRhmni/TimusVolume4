// in his name
// esteghra. sharte badihio check knim doroste! :)

#include <iostream>
#include <vector>
#define pb push_back
using namespace std;

const int maxn = 1050 + 10;
vector <int> G[maxn];
int mark[maxn], color = 1, ted[maxn];

void dfs(int u){
	mark[u] = color;
	for (int i = 0; i < G[u].size(); i++)
		if (!mark[G[u][i]])
			dfs(G[u][i]);
}

int main(){
	
	int u, v;
	while (cin >> u){
		cin >> v;
		G[u].pb(v);
		G[v].pb(u);
	}

	for (int i = 0; i < maxn; i++)
		if (!mark[i]){
			dfs(i);
			color++;
		}

	for (int i = 0; i < maxn; i++)
		for (int j = 0; j < G[i].size(); j++)
			ted[mark[G[i][j]]]++;

	for (int i = 0; i < color; i++)
		if (ted[i] % 4 != 0){
			cout << "0\n";
			return 0;
		}

	cout << "1\n";
	return 0;

}