// in his name

#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#define pb push_back
using namespace std;

vector <int> G[1010];
int n, tah[1010], mark[1010], dis[1010], sz[1010], X[1010], Y[1010];


void bfs(int u){
	queue <int> Q;
	Q.push(u);
	dis[u] = 0;

	while (Q.size()){
		int u = Q.front();
		Q.pop();
		mark[u] = 3;
		X[u] = 2 * dis[u];
		Y[u] = 2 * tah[dis[u]]++;
		for (int i = 0; i < G[u].size(); i++)
			if (mark[G[u][i]] != 3){
				dis[G[u][i]] = dis[u] + 1;
				Q.push(G[u][i]);
			}
	}
}

void dfs(int u){
	mark[u] = 1;
	for (int i = 0; i < G[u].size(); i++){
		if (!mark[G[u][i]]){
			dfs(G[u][i]);
			sz[u] += sz[G[u][i]];
		}
	}
	sz[u]++;
}

int centroid(int u){
	mark[u] = 2;
	for (int i = 0; i < G[u].size(); i++){
		if (mark[G[u][i]] != 2 and sz[G[u][i]] >= (n / 2))
			return centroid(G[u][i]);
	}
	return u;
}

int main(){
	cin >> n;
	for (int i = 0; i < n - 1; i++){
		int u, v;
		cin >> u >> v;
		u--, v--;
		G[u].pb(v);
		G[v].pb(u);
	}

	dfs(0);
	int u = centroid(0);
	bfs(u);

	for (int i = 0; i < n; i++)
		cout << X[i] - 1000 << ' ' << Y[i] - 1000 << endl;

	return 0;
}