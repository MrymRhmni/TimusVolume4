// in his name
// tired of waiting for that silly day! A.A.
// ten million! not one million!

#include <iostream>
#include <vector>
#include <set>
using namespace std;
#define pb push_back
typedef pair<int,int> pii;

const int maxM = 2500 * 100 + 100, inf = 1000 * 1000 * 1000 + 100;
vector <pii> G[510];
int n, m, dis[510], mark[510], col = 0, U[maxM], V[maxM], T[maxM], W[maxM];

bool dijk(){

	set <pii> seti;
	for (int i = 1; i < n; i++){
		seti.insert(pii(inf, i));
		dis[i] = inf;
	}
	dis[0] = 0;
	seti.insert(pii(0, 0));

	while (seti.size()){
		pii p = *seti.begin();
		seti.erase(seti.begin());
		int u = p.second;
		if (p.first > 1440 || u == n - 1) 
			break;
		
		mark[u] = col;
		for (int i = 0; i < G[u].size(); i++){
			int v = G[u][i].second;
			if (mark[v] != col and dis[v] > dis[u] + G[u][i].first){
				seti.erase(pii(dis[v], v));
				dis[v] = dis[u] + G[u][i].first;
				seti.insert(pii(dis[v], v));
			}
		}
	}
	
	return (dis[n - 1] <= 1440);
}

bool pos(int t){

	for (int i = 0; i < n; i++)
		while (G[i].size())
			G[i].pop_back();
		
	col++;

	for (int i = 0; i < m; i++)
		if (W[i] >= 3000 * 1000 + t * 100){
			G[U[i]].pb(pii(T[i], V[i]));
			G[V[i]].pb(pii(T[i], U[i]));
		}

	return dijk();
}

int bin_srch(int st, int en){
	if (en - st <= 1)
		return st;
	int mid = (st + en) / 2;

	if (pos(mid))
		return bin_srch(mid, en);
	else
		return bin_srch(st, mid);
}

int main(){
	cin >> n >> m;
	for (int i = 0; i < m; i++){
		cin >> U[i] >> V[i] >> T[i] >> W[i];
		U[i]--,
		V[i]--;
	}

	cout << bin_srch(0, 1000 * 1000 * 10 + 1) << endl;
	return 0;
}