// in his name

#include <iostream>
using namespace std;
#define pb push_back
#include <vector>
typedef pair<int,int> pii;

vector <pii> V[210];
int A[200][200];

int main(){
	int n;
	cin >> n;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			V[i + j].pb(pii(i, j));

	int k = 0;

	for (int i = 0; i < 2 * n; i++)
		for (int j = 0; j < V[i].size(); j++){
			pii P = V[i][j];
			A[P.first][P.second] = ++k;
		}

	for (int i = 0; i < n; i++, cout << endl)
		for (int j = n - 1; j >= 0; j--)
			cout << A[i][j] << ' ' ;


	return 0;
}