#include <iostream>
using namespace std;
	
int S[1000 * 10 + 100];

int main(){
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++){
		int a;
		cin >> a;
		S[i] = S[i - 1] + a;
	}

	int q;
	cin >> q;
	while (q--){
		int a, b;
		cin >> a >> b;
		cout << S[b] - S[a - 1] << endl;
	}
}