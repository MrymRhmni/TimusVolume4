#include <iostream>
using namespace std;

int dpis[100][20][10], mark[100][20][10];


int dp(int S, int r, int e){
	if (S < 0) return 0;
	if (mark[S][r][e]) return dpis[S][r][e];
	mark[S][r][e] = 1;
	if (r == 1){
		if (e == S)
			dpis[S][r][e] = 1;
		else
			dpis[S][r][e] = 0;
		return dpis[S][r][e];
	}

	for (int i = 0; i <= 9; i++)
		dpis[S][r][e] += dp(S - e, r - 1, i);

	return dpis[S][r][e];
}

int main(){
	
	int S, ans = 0;
	cin >> S;

	if (S == 1){
		cout << 10 ;
		return 0;
	}

	for (int i = 1; i <= 9; i++){
		for (int j = 1; j <= 9; j++)
			ans += dp(S, i, j);
	}

	cout << ans << endl;
	return 0;
}