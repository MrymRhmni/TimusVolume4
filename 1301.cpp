// in his name
// kasif b nazar miad
// U've left a stain in every single hour!

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;
typedef pair<int,int> pi;
typedef pair<pi,pi> pii;
typedef pair<int, pii> piii;
#define F first
#define S second
#define pb push_back

int n, m, A, B, C, D, WallV[20][20], WallH[20][20], inf = 1000 * 1000 * 1000;
int mark[12][12][8][8], dis[12][12][8][8];

int dour[7][4] = {
				{0, 0, 0, 0},
				{2, 3, 4, 5}, 
				{6, 3, 1, 5}, 
				{1, 2, 6, 4}, 
				{1, 3, 6, 5}, 
				{1, 4, 6, 2}, 
				{2, 5, 4, 3}
				};

int backis[] = {0, 6, 4, 5, 2, 3, 1};

vector <pii> V[12][12][8][8];
set <piii> seti;

void make_graph(){
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			for (int U = 1; U <= 6; U++) // Up
				for (int f = 0; f < 4; f++){ // Front
					int Fis = dour[U][f];
					if (i == n - 1 and j == m - 1) break;
					if (!WallV[i][j]){
						int Uright = backis[dour[U][(f + 1) % 4]];
						int Fright = Fis;
						V[i + 1][j][Uright][Fright].pb(pii(pi(i, j), pi(U, Fis)));
						V[i][j][U][Fis].pb(pii(pi(i + 1, j), pi(Uright, Fright)));
					}
					if (!WallH[i][j]){
						int Udown = backis[Fis];
						int Fdown = U;
						V[i][j + 1][Udown][Fdown].pb(pii(pi(i, j), pi(U, Fis)));
						V[i][j][U][Fis].pb(pii(pi(i, j + 1), pi(Udown, Fdown)));
					}
				}
}


void dijk(){
	for (int i = 0; i < 12; i++)
		for (int j = 0; j < 12; j++)
			for (int k = 0; k < 8; k++)
				for (int l = 0; l < 8; l++){
					dis[i][j][k][l] = inf;
					seti.insert(piii(inf, pii(pi(i, j), pi(k, l))));
				}

	seti.erase(piii(inf, pii(pi(A, B), pi(1, 2))));
	dis[A][B][1][2] = 0;
	seti.insert(piii(0, pii(pi(A, B), pi(1, 2))));

	while (seti.size()){
		piii P = *seti.begin();
		seti.erase(seti.begin());
		if (P.first == inf) return;
		pii U = P.S;
		int Q = U.F.F;
		int	W = U.F.S;
		int	E = U.S.F;
		int	R = U.S.S;
		mark[Q][W][E][R] = 1;
		for (int i = 0; i < V[Q][W][E][R].size(); i++){
			pii T = V[Q][W][E][R][i];
			if (!mark[T.F.F][T.F.S][T.S.F][T.S.S] 
				&& dis[T.F.F][T.F.S][T.S.F][T.S.S] > dis[Q][W][E][R] + 1){
				seti.erase(piii(dis[T.F.F][T.F.S][T.S.F][T.S.S], 
								pii(pi(T.F.F, T.F.S), pi(T.S.F, T.S.S))));
				dis[T.F.F][T.F.S][T.S.F][T.S.S] = dis[Q][W][E][R] + 1;
				seti.insert(piii(dis[T.F.F][T.F.S][T.S.F][T.S.S], 
								pii(pi(T.F.F, T.F.S), pi(T.S.F, T.S.S))));
			}
		}
	}

	return;
}

int main(){

	cin >> n >> m >> A >> B >> C >> D;
	A--, B--, C--, D--;

	char c;
	int code = 0;

	while (cin >> c){
		if (c == 'v'){
			code = 1;
			continue;
		}
		if (c == 'h'){
			code = 2;
			continue;
		}

		int k = (int)(c - '0');
		int l;
		cin >> l;
		
		k--, l--;

		if (code == 1)
			WallV[k][l] = 1;
		else
			WallH[k][l] = 1;
	}

	make_graph();
	dijk();

	int ans = inf;

	int U = 1;
		for (int f = 0; f < 4; f++){ // Front
			int Fis = dour[U][f];
			ans = min(ans, dis[C][D][U][Fis]);
		}

	if (ans < inf)
		cout << ans << '\n';
	else
		cout << "No solution\n";

	return 0;
}


