#include <iostream>
using namespace std;

int main(){
	int a, b, k = 0;
	cin >> a >> b;

	for (int i = a; i <= b; i++)
		if (i % 2)
			k++;

	cout << k << endl;
	return 0;
}